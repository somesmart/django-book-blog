# Django-book-blog README

## Django v 1.5.8

## Current Installed Apps:
  - django.contrib.auth
  - django.contrib.contenttypes
  - django.contrib.sessions
  - django.contrib.sites
  - django.contrib.comments
  - django.contrib.messages
  - django.contrib.staticfiles
  - django.contrib.admin
  - tagging
  - mptt
  - zinnia
  - appconf
  - imagekit
  - contact_form

## Python environment requirements:
  - BeautifulSoup
  - enchant
  - markdown